#pragma once

#include "glad.h"
#include <glm/glm.hpp>

namespace lgl
{
class Texture;
class ShaderProgram final
{
public:
	ShaderProgram(const ShaderProgram&) = delete;
	ShaderProgram& operator=(const ShaderProgram&) = delete;
	ShaderProgram(ShaderProgram&&) = delete;
	ShaderProgram& operator=(ShaderProgram&&) = delete;
	virtual ~ShaderProgram();

	ShaderProgram(const GLchar* vsSource, const GLchar* fsSource);

public:
	void enable();
	void disable();
	void setBool(const char* name, bool value) const;
	void setInt(const char* name, int value) const;
	void setFloat(const char* name, float value) const;
	void setMatrix4f(const char* name, const glm::mat4& mat);
	void bindTexture(const char* name, const Texture& tex, int slot) const;

private:
	GLuint m_id;
};
} // namespace lgl
