#pragma once

#include "glad.h"

namespace lgl
{
class Texture final
{
public:
	Texture(const char* filePath, bool generateMips);
	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;
	Texture(Texture&&) = delete;
	Texture& operator=(Texture&&) = delete;
	virtual ~Texture();

public:
	bool hasMips() const
	{
		return m_hasMips;
	}

	int width() const
	{
		return m_width;
	}

	int height() const
	{
		return m_height;
	}

	int channels() const
	{
		return m_channels;
	}

	GLuint getId() const
	{
		return m_id;
	}

private:
	bool m_hasMips;
	int m_width, m_height;
	int m_channels;
	GLuint m_id;
}; // namespace glclassTexturefinal
} // namespace lgl
