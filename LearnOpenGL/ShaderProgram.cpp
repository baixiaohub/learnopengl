#include "ShaderProgram.h"

#include "Texture.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cassert>
#include <cstdio>

namespace
{ // utils
GLuint createShader(GLenum shaderType, const char* source)
{
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	int success = GL_NO_ERROR;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char log[512];
		glGetShaderInfoLog(shader, sizeof(log), NULL, log);
		printf("Compile Shader Failed:%s\n", log);
		assert(0);
	}

	return shader;
}

GLuint createProgram(GLuint vs, GLuint fs)
{
	GLuint program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);

	int success = GL_NO_ERROR;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success)
	{
		char log[512];
		glGetProgramInfoLog(program, sizeof(log), NULL, log);
		printf("Link Program Failed:%s\n", log);
		assert(0);
	}

	return program;
}

GLuint createProgram(const char* vsSource, const char* fsSource)
{
	assert(vsSource);
	assert(fsSource);

	GLuint vs = createShader(GL_VERTEX_SHADER, vsSource);
	GLuint fs = createShader(GL_FRAGMENT_SHADER, fsSource);
	GLuint program = createProgram(vs, fs);

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}
} // namespace

namespace lgl
{
ShaderProgram::ShaderProgram(const GLchar* vsSource, const GLchar* fsSource)
    : m_id(0)
{
	m_id = createProgram(vsSource, fsSource);
}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(m_id);
	m_id = 0;
}

void ShaderProgram::enable()
{
	assert(m_id != 0 && "ShaderProgram not created!");
	glUseProgram(m_id);
}

void ShaderProgram::disable()
{
	glUseProgram(0);
}

void ShaderProgram::setBool(const char* name, bool value) const
{
	assert(name);
	auto location = glGetUniformLocation(m_id, name);
	glUniform1i(location, value ? 1 : 0);
}

void ShaderProgram::setInt(const char* name, int value) const
{
	assert(name);
	auto location = glGetUniformLocation(m_id, name);
	glUniform1i(location, value);
}

void ShaderProgram::setFloat(const char* name, float value) const
{
	assert(name);
	auto location = glGetUniformLocation(m_id, name);
	glUniform1f(location, value);
}

void ShaderProgram::setMatrix4f(const char* name, const glm::mat4& mat)
{
	assert(name);
	auto location = glGetUniformLocation(m_id, name);
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
}

void ShaderProgram::bindTexture(const char* name, const Texture& tex,
                                int slot) const
{
	assert(name);
	setInt(name, slot);

	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, tex.getId());
}
} // namespace lgl
