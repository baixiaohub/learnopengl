#include "ShaderProgram.h"
#include "Texture.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "glad.h"
#include <GLFW/glfw3.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>

namespace globals
{
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;
} // namespace globals

namespace
{ // data
struct Vec2 final
{
	float x, y;
};

struct Vec3 final
{
	float x, y, z;
};

struct Vertex final
{
	Vec3 pos;
	Vec2 uv;
};

struct Camera final
{
	float fov;

	glm::vec3 pos;
	glm::vec3 front;
	glm::vec3 up;
};

Camera camera = {
  45.0f,
  glm::vec3(0.0f, 0.0f, 3.0f),
  glm::vec3(0.0f, 0.0f, -1.0f),
  glm::vec3(0.0f, 1.0f, 0.0f),
};

Vertex vertices[] = {
  // pos, uv.
  -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 0.5f,  -0.5f, -0.5f, 1.0f, 0.0f,
  0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
  -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 0.0f,

  -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, 0.5f,  -0.5f, 0.5f,  1.0f, 0.0f,
  0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
  -0.5f, 0.5f,  0.5f,  0.0f, 1.0f, -0.5f, -0.5f, 0.5f,  0.0f, 0.0f,

  -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, -0.5f, 0.5f,  -0.5f, 1.0f, 1.0f,
  -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
  -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, -0.5f, 0.5f,  0.5f,  1.0f, 0.0f,

  0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
  0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,  -0.5f, -0.5f, 0.0f, 1.0f,
  0.5f,  -0.5f, 0.5f,  0.0f, 0.0f, 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

  -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.5f,  -0.5f, -0.5f, 1.0f, 1.0f,
  0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, 0.5f,  -0.5f, 0.5f,  1.0f, 0.0f,
  -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,

  -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, 0.5f,  0.5f,  -0.5f, 1.0f, 1.0f,
  0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
  -0.5f, 0.5f,  0.5f,  0.0f, 0.0f, -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f,
};

uint32_t indices[] = {0, 1, 2, 0, 2, 3};

glm::vec3 cubePositions[] = {
  glm::vec3(0.0f, 0.0f, 0.0f),    glm::vec3(2.0f, 5.0f, -15.0f),
  glm::vec3(-1.5f, -2.2f, -2.5f), glm::vec3(-3.8f, -2.0f, -12.3f),
  glm::vec3(2.4f, -0.4f, -3.5f),  glm::vec3(-1.7f, 3.0f, -7.5f),
  glm::vec3(1.3f, -2.0f, -2.5f),  glm::vec3(1.5f, 2.0f, -2.5f),
  glm::vec3(1.5f, 0.2f, -1.5f),   glm::vec3(-1.3f, 1.0f, -1.5f),
};

const char* vs = R"(
	#version 330 core
	layout (location = 0) in vec3 pos;
	layout (location = 1) in vec2 uv;

	out vec2 texCoord;

	uniform mat4 model;
	uniform mat4 view;
	uniform mat4 projection;

	void main()
	{
		gl_Position = projection * view * model * vec4(pos, 1.0);
		texCoord = uv;
	}
)";

const char* fs = R"(
	#version 330 core
	out vec4 FragColor;
	in vec2 texCoord;

	uniform sampler2D containerTex;
	uniform sampler2D faceTex;

	void main()
	{
		vec4 containerTexColor = texture(containerTex, texCoord);
		vec4 faceTexColor = texture(faceTex, texCoord);
		FragColor = mix(containerTexColor, faceTexColor, 0.5);
	}
)";
} // namespace

namespace
{ // callbacks
inline float clamp(float value, float min, float max)
{
	return std::max(std::min(value, max), min);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	float xposf = (float)xpos;
	float yposf = (float)ypos;

	static float lastX = 0.5f * globals::WINDOW_WIDTH;
	static float lastY = 0.5f * globals::WINDOW_HEIGHT;

	float xoffset = xposf - lastX;
	float yoffset = lastY - yposf;
	lastX = xposf;
	lastY = yposf;

	const float sensitivity = 0.2f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	static float yaw = 180.0f;
	static float pitch = 0.0f;
	static float roll = 0.0f;
	yaw += xoffset;
	pitch += yoffset;
	pitch = clamp(pitch, -89.0f, 89.0f);

	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	camera.front = glm::normalize(front);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	float xoffsetf = (float)xoffset;
	float yoffsetf = (float)yoffset;

	float fov = camera.fov;
	if (fov >= 1.0f && fov <= 45.0f)
	{
		fov -= yoffsetf;
	}
	camera.fov = clamp(fov, 1.0f, 45.0f);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	static float lastTime = 0.0f;
	float deltaTime = 0.0f;
	float currentTime = (float)glfwGetTime();
	deltaTime = currentTime - lastTime;
	lastTime = currentTime;

	float cameraSpeed = 2.5f * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		cameraSpeed *= 3.0f;
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	{
		camera.pos += cameraSpeed * camera.front;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		camera.pos -= cameraSpeed * camera.front;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		camera.pos -=
		  glm::normalize(glm::cross(camera.front, camera.up)) * cameraSpeed;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		camera.pos +=
		  glm::normalize(glm::cross(camera.front, camera.up)) * cameraSpeed;
	}
}

void printGPUCaps()
{
	int nrAttributes = 0;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	printf("Maximum nr of vertex attributes supported:%d\n", nrAttributes);
}
} // namespace

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(
	  globals::WINDOW_WIDTH, globals::WINDOW_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		printf("Failed to create GLFW window\n");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		printf("Failed to initialize GLAD\n");
		return -1;
	}
	printGPUCaps();

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glViewport(0, 0, 800, 600);
	// glEnable(GL_SCISSOR_TEST);
	// glScissor(400, 300, 400, 300);
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

	lgl::ShaderProgram defaultProgram(vs, fs);
	lgl::Texture containerTex("images/container.jpg", true);
	lgl::Texture faceTex("images/awesomeface.png", true);

	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	GLuint VBO;
	glGenBuffers(1, &VBO);
	GLuint IBO;
	glGenBuffers(1, &IBO);

	{ // fill buffers.
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices,
		             GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
		             GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	{ // set vbo vertex format.
		// ref:https://www.khronos.org/opengl/wiki/Vertex_Specification

		// Think of it like this.glBindBuffer sets a global variable,
		//  then glVertexAttribPointer reads that global variable and stores it
		//  in the VAO. Changing that global variable after it 's been read
		//  doesn' t affect the VAO.You can think of it that way because that's
		//  exactly how it works.
		//  This is also why GL_ARRAY_BUFFER is not VAO state;
		// the actual association between an attribute index and a buffer is
		// made
		//  by glVertexAttribPointer.
		//  Note that it is an error to call the glVertexAttribPointer
		//    functions if 0 is currently bound to
		//      GL_ARRAY_BUFFER.
		glBindVertexArray(VAO);
		{
			glBindBuffer(GL_ARRAY_BUFFER, VBO);

			int location = 0;
			int stride = sizeof(Vertex);

			glVertexAttribPointer(location, sizeof(Vertex::pos) / sizeof(float),
			                      GL_FLOAT, GL_FALSE, stride,
			                      (void*)(offsetof(Vertex, pos))); // pos
			glEnableVertexAttribArray(location);
			++location;

			glVertexAttribPointer(location, sizeof(Vertex::uv) / sizeof(float),
			                      GL_FLOAT, GL_FALSE, stride,
			                      (void*)(offsetof(Vertex, uv))); // uv
			glEnableVertexAttribArray(location);
			++location;

			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		glBindVertexArray(0);
	}

	{ // default states.
		float borderColor[] = {1.0f, 1.0f, 0.0f, 1.0f};
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		glEnable(GL_DEPTH_TEST);
	}

	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		// update variables.
		float time = (float)glfwGetTime();
		// float greenValue = (sinf(time) / 2.0f) + 0.5f;
		// int vertexColorUniformLocation =
		//  glGetUniformLocation(defaultProgram, "outColor");
		// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		defaultProgram.enable(); // glUseProgram(defaultProgram);
		// glUniform4f(vertexColorUniformLocation, 0.0f, greenValue, 0.0f,
		//            1.0f);
		defaultProgram.bindTexture("containerTex", containerTex, 0);
		defaultProgram.bindTexture("faceTex", faceTex, 1);

		glm::mat4 model = glm::mat4(1.0);
		model = glm::rotate(model, time * glm::radians(50.0f),
		                    glm::vec3(1.0f, 1.0f, 0.0f));
		glm::mat4 view = glm::mat4(1.0);
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));

		glm::mat4 projection = glm::perspective(
		  glm::radians(45.0f),
		  1.0f * globals::WINDOW_WIDTH / globals::WINDOW_HEIGHT, 0.1f, 100.0f);

		defaultProgram.setMatrix4f("model", model);
		defaultProgram.setMatrix4f("view", view);
		defaultProgram.setMatrix4f("projection", projection);

		{ // draw
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glBindVertexArray(VAO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

			int cubeCount = sizeof(cubePositions) / sizeof(cubePositions[0]);
			for (int i = 0; i < cubeCount; ++i)
			{
				glm::mat4 model = glm::mat4(1.0);
				model = glm::translate(model, cubePositions[i]);

				float angle = 20.0f * i;
				model = glm::rotate(model, time * glm::radians(angle),
				                    glm::vec3(1.0f, 0.3f, 0.5f));
				defaultProgram.setMatrix4f("model", model);

				glm::mat4 view =
				  glm::lookAt(camera.pos, camera.front + camera.pos, camera.up);
				defaultProgram.setMatrix4f("view", view);

				glm::mat4 projection = glm::perspective(
				  glm::radians(camera.fov),
				  1.0f * globals::WINDOW_WIDTH / globals::WINDOW_HEIGHT, 0.1f,
				  100.0f);
				defaultProgram.setMatrix4f("projection", projection);

				glDrawArrays(GL_TRIANGLES, 0,
				             sizeof(vertices) / sizeof(vertices[0]));
			}
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
