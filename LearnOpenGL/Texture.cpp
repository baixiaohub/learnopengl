#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <cassert>
#include <memory>

namespace lgl
{
Texture::Texture(const char* filePath, bool generateMips)
    : m_hasMips(generateMips), m_width(0), m_height(0), m_channels(0), m_id(0)
{
	assert(filePath);

	stbi_set_flip_vertically_on_load(true);
	stbi_uc* data = stbi_load(filePath, &m_width, &m_height, &m_channels, 0);
	if (!data)
	{
		printf("load texture failed: %s\n", stbi_failure_reason());
	}
	assert(data && "load texture failed!");
	assert((m_channels == 3 || m_channels == 4)
	       && "Only support RGB/RGBA texture.");

	{
		glGenTextures(1, &m_id);

		glBindTexture(GL_TEXTURE_2D, m_id);
		// set the texture wrapping/filtering options (on the currently
		// bound texture object)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		GLenum dataType = (m_channels == 3) ? GL_RGB : GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, dataType, m_width, m_height, 0, dataType,
		             GL_UNSIGNED_BYTE, data);
		if (generateMips)
		{
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	stbi_image_free(data);
}

Texture::~Texture()
{
	if (m_id)
	{
		glDeleteTextures(1, &m_id);
	}
}
} // namespace lgl
